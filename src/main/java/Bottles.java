package assignment2;
class Bottles {
    public String verse(int verseNumber) {
        int verseNumber2 = verseNumber - 1;

        if ( verseNumber <= 99 && verseNumber > 2 ) {
            return verseNumber + " bottles of beer on the wall, " + verseNumber + " bottles of beer.\n"
                    + "Take one down and pass it around, " + verseNumber2 + " bottles of beer on the wall.\n";
        }

        if (verseNumber == 3) {
            return verseNumber + " bottles of beer on the wall, " + verseNumber + " bottles of beer.\n" + "Take one down and pass it around, " + verseNumber2 + " bottles of beer on the wall.\n";
        }
        if (verseNumber == 2) {
            return verseNumber + " bottles of beer on the wall, " + verseNumber + " bottles of beer.\n" + "Take one down and pass it around, " + verseNumber2 + " bottle of beer on the wall.\n";
        }
        if (verseNumber == 1) {
            return verseNumber + " bottle of beer on the wall, " + verseNumber + " bottle of beer.\n" + "Take it down and pass it around, "
                    + "no more bottles of beer on the wall.\n";
        }
        if (verseNumber == 0) {
            return "No more bottles of beer on the wall, " + "no more bottles of beer.\n"
                    + "Go to the store and buy some more, " + "99 bottles of beer on the wall.\n";
        }

        return "";
    }


    public String verse(int startVerseNumber, int endVerseNumber) {
        if (startVerseNumber == 99 && endVerseNumber == 98) {
            int a = endVerseNumber - 1;
            return startVerseNumber + " bottles of beer on the wall, " + startVerseNumber + " bottles of beer.\n"
                    + "Take one down and pass it around, " + endVerseNumber + " bottles of beer on the wall.\n" + "\n"
                    + endVerseNumber + " bottles of beer on the wall, " + endVerseNumber + " bottles of beer.\n" + "Take one down and pass it around, "
                    + a + " bottles of beer on the wall.\n";
        }

        if (startVerseNumber == 2 && endVerseNumber == 0) {
            int b = startVerseNumber - 1;
            return startVerseNumber + " bottles of beer on the wall, " + startVerseNumber + " bottles of beer.\n"
                    + "Take one down and pass it around, " + b + " bottle of beer on the wall.\n" + "\n"
                    + b + " bottle of beer on the wall, " + b + " bottle of beer.\n" + "Take it down and pass it around, "
                    + "no more bottles of beer on the wall.\n" + "\n" + "No more bottles of beer on the wall, "
                    + "no more bottles of beer.\n" + "Go to the store and buy some more, "
                    + "99 bottles of beer on the wall.\n";
        }
        return "";
    }

    public String song() {
        String song = verse(99);
        int firstNum = 98;
        int lastNum = 0;
        for (int i = firstNum; i >= lastNum; i--) {
            song = song + "\n" + verse(i);
        }
        return song;
    }
}